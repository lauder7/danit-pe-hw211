// Initializing
idForm.onsubmit = hndPasswordSubmit;

for (let de of document.querySelectorAll('.passwordshow')) {
    de.style.display = "inline";
    de.addEventListener("click", hndPasswordShowHide);
}

for (let de of document.querySelectorAll('.passwordhide')) {
    de.style.display = "none";
    de.addEventListener("click", hndPasswordShowHide);
}



function hndPasswordSubmit(ev) {
    if (idPasswordBox.value && idPasswordBox.value === idPasswordConfirmBox.value)
        alert("You are welcome!");
    else
        alert("Please enter the same values");
    ev.preventDefault();
}



function hndPasswordShowHide(ev) {
    if (ev.currentTarget.classList.contains("passwordshow"))
        document.getElementById(ev.currentTarget.dataset.pwdboxid).type = "text";
    else if (ev.currentTarget.classList.contains("passwordhide"))
        document.getElementById(ev.currentTarget.dataset.pwdboxid).type = "password";
        
    ev.currentTarget.style.display = "none";
    document.getElementById(ev.currentTarget.dataset.trigid).style.display = "inline";
}
